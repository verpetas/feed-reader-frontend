import angular from 'angular';

import {FeedService} from './app/feed/feed';
import {App} from './app/containers/app';
import {Header} from './app/components/header';
import {FeedList} from './app/components/feedList';
import {HistoryList} from './app/components/historyList';
import 'angular-ui-router';
import 'angular-timeago';
import uiBootstrap from 'angular-ui-bootstrap';
import ngAnimate from 'angular-animate';
import routesConfig from './routes';

import './index.scss';

angular
  .module('app', ['ui.router', 'yaru22.angular-timeago', uiBootstrap, ngAnimate])
  .config(routesConfig)
  .service('feedService', FeedService)
  .component('app', App)
  .component('headerComponent', Header)
  .component('feedList', FeedList)
  .component('historyList', HistoryList);
