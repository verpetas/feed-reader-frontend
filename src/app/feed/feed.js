export class FeedService {
  /** @ngInject */
  constructor($http, $rootScope) {
    this.$http = $http;
    this.$rootScope = $rootScope;
  }

  getFromUrl(url) {
    return this.$http.get('https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20rss%20where%20url%20%3D%20"' + encodeURIComponent(url) + '"&format=json')
      .then(response => {
        this.addToHistory(url);
        return response.data.query.results ? response.data.query.results.item : [];
      });
  }

  addToHistory(url) {
    let history = localStorage.getItem('history');
    history = history === null ? [] : angular.fromJson(history);
    const data = {url, date: Date.now()};
    this.$rootScope.$broadcast('history:added', data);
    history.unshift(data);
    if (history.length > 50) {
      history.pop();
    }
    localStorage.setItem('history', angular.toJson(history));
  }

  getHistory() {
    const history = localStorage.getItem('history');
    return history === null ? [] : angular.fromJson(history);
  }
}

