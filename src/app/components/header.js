class HeaderController {
  /** @ngInject */
  constructor($rootScope) {
    this.$rootScope = $rootScope;
    this.text = '';
  }

  change() {
    if (this.text.length !== 0) {
      this.$rootScope.$broadcast('search:changed', this.text);
    }
  }
}

export const Header = {
  template: require('./header.html'),
  controller: HeaderController
};
