class HistoryListController {
  /** @ngInject */
  constructor(feedService, $scope) {
    this.feedService = feedService;
    this.$scope = $scope;
    this.history = [];
    this.init();
  }

  init() {
    this.history = this.feedService.getHistory();
    this.$scope.$on('history:added', (event, data) => {
      this.history.unshift(data);
    });
  }

  encodeUrl(url) {
    return encodeURIComponent(url);
  }
}

export const HistoryList = {
  template: require('./historyList.html'),
  controller: HistoryListController
};
