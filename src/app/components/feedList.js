import ModalWinController from './modalWin';

class FeedListController {
  /** @ngInject */
  constructor(feedService, $uibModal, $stateParams, $scope, $window) {
    this.feedService = feedService;
    this.$uibModal = $uibModal;
    this.$scope = $scope;
    this.$window = $window;
    this.$stateParams = $stateParams;
    this.feed = [];
    this.sortBy = 'timestamp';
    this.reverse = true;
    this.busy = false;
    this.filter = '';
    this.init();
  }

  init() {
    this.$scope.$on('search:changed', (event, text) => {
      this.filter = text;
    });
    if (this.$stateParams.url) {
      this.load(this.$stateParams.url);
      this.url = this.$stateParams.url;
      return;
    }
    const history = this.feedService.getHistory();
    if (history.length > 0) {
      this.load(history[0].url);
      this.url = history[0].url;
    }
  }

  submit() {
    if (this.busy) {
      return;
    }
    this.load(this.url);
  }

  load(url) {
    this.busy = true;
    this.feed = [];
    this.feedService.getFromUrl(url)
      .then(data => {
        for (const i in data) {
          if (Object.prototype.hasOwnProperty.call(data, i)) {
            if (data[i].pubDate) {
              data[i].timestamp = this.toTimestamp(data[i].pubDate);
            } else {
              data[i].timestamp = 0;
            }
          }
        }
        this.feed = data;
        this.busy = false;
      }, () => {
        this.busy = false;
      });
  }

  view(item) {
    const modalInstance = this.$uibModal.open({
      animation: true,
      template: require('./modalWin.html'),
      controller: ModalWinController,
      controllerAs: 'vm',
      backdrop: 'static',
      size: 'lg',
      resolve: {
        item
      }
    });

    modalInstance.result.then(() => { // ok
      this.$window.open(item.link, '_blank');
    }, () => { // cancel

    });
  }

  toTimestamp(date) {
    return new Date(date).getTime();
  }

  changeSortBy(field) {
    if (field === this.sortBy) {
      this.reverse = !this.reverse;
    } else {
      this.reverse = true;
    }
    this.sortBy = field;
  }

  search() {
    const self = this;
    return function (item) {
      let result = false;
      if (self.filter.length === 0) {
        result = true;
      } else {
        const val = self.filter.toLowerCase();
        result = item.title.toLowerCase().indexOf(val) > -1;
      }
      return result;
    };
  }
}

export const FeedList = {
  template: require('./feedList.html'),
  controller: FeedListController
};
