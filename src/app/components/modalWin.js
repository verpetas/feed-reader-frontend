export default class ModalWinController {
  /** @ngInject */
  constructor($uibModalInstance, item) {
    this.$uibModalInstance = $uibModalInstance;
    this.item = item;
  }

  ok() {
    this.$uibModalInstance.close();
  }

  cancel() {
    this.$uibModalInstance.dismiss('cancel');
  }
}
