class AppController {
  /** @ngInject */
  constructor($log) {
    this.$log = $log;
  }
}

export const App = {
  template: require('./app.html'),
  controller: AppController
};
